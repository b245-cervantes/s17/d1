// console.log("Hello World");

//[SECTION] Functions 
 // Functions in javascript are line/blocks of codes
 // that tell our device/application to perform a specific
 // task when called/invoked.
 // it prevents repeating lines/blocksof codes that perform
 // that same task/fucntion.
 
 // Function Declarations
      //function statement is the definition of a function.

      /*
       Syntax:
          function functionName() {
	         code block (statement)
          }

          - function keyword - used to define a javascript 
          function.
          -functionName - name of the function, which will be
          used to call/invoked the function
          -function block({}) - indicates the function body.



      */

      function printName() {
      	console.log("My name is John");
      }

      // function Invocation
      // this run/execute the code block inside the function
       printName();

       declaredFunction(); // we cannot invoked a function
       // that we have not declared/define yet.

       //[SECTION] Function declarations vs Function Expressions
          //fucntion can be create by using function keyword and
          // adding a function name.
          // "save for later used"
          // Declared function can be "hoisted", as long a function
          // has been defined.
            //Hoisting is a JS behavior for certain variable (var) and functions
            //to run or use them before their declaration.

          function declaredFunction(){
            console.log("Hello World from declaredFunction()");
          }

          // function Expression
             // A function can also be stored in a variable.
             /*
              
               Sysntax:
                let/const variableName = function() {
                     //codeblock (statement)
                }
                    -function(){} Anonymous function, a function
                    without a name.


             */

             //variableFunction(); //error - function expression, being
             // stored in a let/const variable, cannot be hoisted.

             let variableFunction = function() {
               console.log("Hello Again!");
             }
             variableFunction();

             // We can create also functiob expression with a named
             // function.

             let funcExpression = function funcName() {
               console.log("Hello from the other side.");
             }
             //funName(); //funcName() is not defined
             funcExpression(); // to invoke the function, we invoked it
             // by its "variable name" and not by its function name


             //Reassign declared functions and function expression to
             // a new anonymous function

             declaredFunction = function(){
               console.log("updated declaredFunction");
             }
             declaredFunction();

             funcExpression = function() {
               console.log("updated funcExpression");
             }
             funcExpression();

             // we cannot re-assign a function expression initialized
             // with const,

             const constantFunc = function() {
               console.log("Initialized with const");
             }

             constantFunc();

              // this will result to reassignment error
             // constantFunc = function(){
             //   console.log("cannot be reassigned");
             // }
             // constantFunc();

             // [Section] function Scoping

             /*

             Scope is the accessibility (visibility) of variables.

             javascipt variables has 3 types of scope:

             1.global scope
             2.local/block scope
             3.function scope
             */

             //Global Scope
                 // variable can be access anywhere from the program.

                 let globalVar = "Mr. WorldWide";

            //Local Scope
               //Variables declared inside a curly bracelet ({}) can
               // only be accessed locally.
               //console.log(localVar);

               {   
                   //var localVar = "Armando Perez";
                  let localVar = "Armando Perez";
               }

               console.log(globalVar);
               //console.log(localVar);// result in error. cannot be 
              // accessed outside it codeblock.

              // Function Scope
                  // Each Function creates a new scope.
                  // Variables defined inside a function are not
                  // accessible from outside the function.

                  function showNames(){
                     //function Scope Variable
                     var functionVar = "Joe";
                     const functionConst = "John";
                     let functionLet = "Joey";

                     console.log(functionVar);
                     console.log(functionConst);
                     console.log(functionLet);

                  }

                  showNames();

                     //console.log(functionVar);
                     //console.log(functionConst);
                     //console.log(functionLet);

                     function myNewFunction(){
                        let name ="Jane";
                        function nestedFunc(){
                           let nestedName ="John";
                           console.log(name);
                        }
                       // console.log(nestedName);
                        nestedFunc();
                     }
                     myNewFunction();

                    // nestedFunc(); //result to an error

                    //Global Scope Variable
                    let globalName = "Alex";
                    function myNewFunction2(){
                     let nameInside = "Renz";
                     console.log(globalName);
                    }
                    myNewFunction2();
                  // console.log(nameInside) // only accessible on the 
                  //function scope

               //[SECTION] Using alert() and prompt
                 //aler() allows us to show small window at the top of our
                 //browser page to show informationb to our users.


                 //alert("Hellow World"); // This will run imediately when the
                 //page reloads

                 // you can use an alert() to show a message to the user from
                 // a later function invocation.

                 // function showSampleAlert(){
                 //  alert("Hello User");
                 // }
                 // showSampleAlert();

                 console.log("I will log in the  console when the alert is dismissed.");

                 // Notes on the use of aler();
                 // Show only an alert() for short dialogs/messages to the
                 // user.

                 // Do not overuse alert() becuase the program has to wait
                 // for it to be dismissed before it continues.

               // prompt() allow us to show a small window at the top of the browser
               // to gather user input.
               // The input from the input prompt() will returned as a "String"
               //once the user dismissed the window.

               /*
                  Syntax: let/const variableName = prompt("<dialogInString");

               */

               // let name = prompt("Enter your name: ");
               // let age = prompt("Enter your age: ");

               // console.log(typeof age);
               // console.log("hello I am " + name + ", I am " + age +" years old. ");

               // let sampleNullPrompt = prompt("Do not Enter Anything: ");
               // console.log(sampleNullPrompt);
               //prompt() return an "Emty string" ("") when there is no user inpit and we
               // have clicked okay. or "null" if the user cancels the prompt.

               function prinWelcomeMessage(){
                  let name = prompt("Enter your name: ");

                  console.log("hello, " + name + "! Welcome to my page!");
               }
               prinWelcomeMessage();

               //[SECTION] Function Naming Conventions
                 // Function names should be definitive of the task it
                 // will perform. IT usually contains a verb.
                   // function also follows camelCase for naming unctions

                 function getCourses(){
                  let courses = ["Science 101", "Math 101", "English 101"];
                  console.log(courses);
                 }
                 getCourses();

                 // 2. Avoid generic names to avoid confusion within your 
                 //code.

                 function get() {
                  let name ="Jamie";
                  console.log(name);
                 }
                 get();

                 // 3. Avoid pointless and inappropriate function name,
                 //example: foo, bar.

                 function foo(){
                  console.log(25%5)
                 }
                 foo();
